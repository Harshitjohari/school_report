from tkinter import ttk
import tkinter as tk
import psycopg2
from PIL import ImageTk
from PIL import Image,ImageTk
from tkinter import ttk
from tkinter import messagebox
import tkinter as tk
from tkinter import Tk
from tkinter import *
from tkcalendar import *

win=tk.Tk()
win.title("SCHOOL A.I")
def report_():
    root = tk.Tk()
    root.title("School Report")
    root.geometry("{0}x{1}+0+0".format(root.winfo_screenwidth(), root.winfo_screenheight()))
    root.wm_attributes('-type', 'splash')
    root.configure(background='light blue')

    var1=IntVar()
    var2=StringVar()
    var3=IntVar()

    def submit():
        period_id=var3.get()
        class_=var1.get()
        section=var2.get()
        date=entry1.get()
        #print(period_id,class_,section)
        #insert(period_id,class_,section)    
        def insert(period_id,class_,section,date):
            
            conn = psycopg2.connect(database="school", user="harshit", password="1996", host="127.0.0.1", port="5432")
            cursor=conn.cursor()
            cursor.execute("select o.rollno ,o.name ,o.class_ ,o.section,(select p.period_id from period p  where p.period_id ='"+str(period_id)+"'),a.date , count(a.activity) ,a.activity ,(max(time )-min(time )) presence from attendance a LEFT JOIN students o ON o.s_id = a.s_id_fk  where a.s_id_fk  in (select s2.s_id  from  students s2 where s2.class_ ='"+str(class_)+"' and s2.section ='"+str(section)+"'  ) and a.date ='"+str(date)+"' and  a.time >  (select p.period_start from period p  where p.period_id ='"+str(period_id)+"') and a.time <(select p.period_end from period p  where p.period_id ='"+str(period_id)+"') group by o.s_id ,a.date,a.activity")
            #cursor.execute("select o.rollno ,o.name ,o.class_ ,o.section,(select p.period_id from period p  where p.period_id =6),a.date  from attendance a LEFT JOIN students o ON o.s_id = a.s_id_fk where a.s_id_fk  in (select s2.s_id  from  students s2 where s2.class_ ='12' and s2.section ='A'  ) and a.date ='20/02/20' and  a.time >  (select p.period_start from period p  where p.period_id =6) and a.time <(select p.period_end from period p  where p.period_id =6) group by o.rollno ,o.name ,o.class_ ,o.section ,a.date")
            rows = cursor.fetchall()
            for row in rows:
                tree.insert('', 'end', values=(row[0],row[1], row[2], row[3],row[4],row[5],row[7],row[8]))
            conn.commit()
            conn.close()
        insert(period_id,class_,section,date)      


    tree= ttk.Treeview(root,height=40 ,column=("column1", "column2", "column3","column4","column5","column6","column7","column8"), show='headings')
    tree.heading("#1",text="Roll No.")
    tree.heading("#2", text="Name")
    tree.heading("#3", text="Class")
    tree.heading("#4", text="Section")
    tree.heading("#5",text="Period")
    tree.heading("#6",text="Date")
    tree.heading("#7",text="Activity")
    tree.heading("#8",text="Presence")
    #tree.heading("#8",text="Presence Duration")
    tree.place(x=95,y=110)

    ###select role##
    label5 = Label(root, text='Role: Student',font=("times new roman",22,"bold"),bg="light blue",relief=GROOVE)
    label5.place(x=30,y=30)


    ###########class##########
    label1 = Label(root, text='Class: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
    label1.place(x=270,y=30)
    list1=['1','2','3','4','5','6','7','8','9','10','11','12']
    droplist1=OptionMenu(root,var1,*list1)
    droplist1.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
    var1.set('....')    
    droplist1.place(x=340,y=30)

    ######section#######
    label2 = Label(root, text='Section: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
    label2.place(x=460,y=30)
    list2=['A','B','C','D']
    droplist2=OptionMenu(root,var2,*list2)
    droplist2.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
    var2.set('....')    
    droplist2.place(x=550,y=30)

    ###########period##########
    label3 = Label(root, text='Period: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
    label3.place(x=670,y=30)
    list3=['1','2','3','4','5','6','7','8']
    droplist3=OptionMenu(root,var3,*list3)
    droplist3.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
    var3.set('....')    
    droplist3.place(x=750,y=30)

    ###########date#########
    label4 = Label(root, text='Date: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
    label4.place(x=870,y=30)
    entry1 =Entry(root,width=20,bd=2)
    entry1.place(x=930,y=30)
    label5 = Label(root, text='Date Format =dd/mm/yy',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
    label5.place(x=1100,y=30)



    #submit
    Button1=Button(root, text="Submit",command=submit,font=("times new roman",12,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
    Button1.place(x=1700,y=30)

    #back
    Button3=Button(root,text="Back", command=root.destroy,font=("times new roman",18,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
    Button3.place(x=1800,y=30) 

    #######reset###########
    def reset_values():
        entry1.delete(0, 'end')    
        
    Button4=Button(root,text="Reset", command=reset_values,font=("times new roman",18,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
    Button4.place(x=1800,y=170) 


    ###teacher report####################################################################################################################
    def teacher_report():
        root1 = tk.Tk()
        root1.title("School Report")
        root1.geometry("{0}x{1}+0+0".format(root1.winfo_screenwidth(), root1.winfo_screenheight()))
        root1.wm_attributes('-type', 'splash')
        root1.configure(background='light blue')
        var6=IntVar()


        def submit1():
            date=entry2.get()
            period_id=var6.get()
            def insert_tec(period_id,date):
                conn = psycopg2.connect(database="school", user="harshit", password="1996", host="127.0.0.1", port="5432")
                cursor=conn.cursor()
                cursor.execute("select t.t_id ,t.t_name ,(select p.period_id from period p  where p.period_id ='"+str(period_id)+"'),ta.date , count(ta.activity ) ,ta.activity ,(max(time )-min(time )) presence from t_attendance ta LEFT JOIN teachers t ON t.id =ta.t_id_fk where ta.t_id_fk in (select t2.id  from  teachers t2  )and ta.date ='"+str(date)+"' and  ta.time >  (select p.period_start from period p  where p.period_id ='"+str(period_id)+"') and ta.time <(select p.period_end from period p  where p.period_id ='"+str(period_id)+"') group by t.id ,ta.date,ta.activity")
                rows = cursor.fetchall()
                for row in rows:
                    tree.insert('', 'end', values=(row[0],row[1], row[2], row[3],row[5],row[6]))
                conn.commit()
                conn.close()

            insert_tec(period_id,date) 





        tree= ttk.Treeview(root1,height=40 ,column=("column1", "column2", "column3","column4","column5","column6"), show='headings')
        tree.heading("#1",text="Teacher ID")
        tree.heading("#2", text="Name")
        tree.heading("#3", text="Period")
        tree.heading("#4",text="Date")
        tree.heading("#5",text="Activity")
        tree.heading("#6",text="Presence")
        tree.place(x=150,y=110)
        
        ###select role##
        label5 = Label(root1, text='Role: Teacher',font=("times new roman",22,"bold"),bg="light blue",relief=GROOVE)
        label5.place(x=30,y=30)
        
        ###########period##########
        label3 = Label(root1, text='Period: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label3.place(x=250,y=30)
        list6=['1','2','3','4','5','6','7','8']
        droplist6=OptionMenu(root1,var6,*list6)
        droplist6.config(width=7,font=("times new roman",15,"bold"),bg="light blue",activebackground="grey",activeforeground="light blue",relief=GROOVE)
        var6.set('....')    
        droplist6.place(x=330,y=30)
        
        ##date##
        label4 = Label(root1, text='Date: ',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label4.place(x=470,y=30)
        entry2 =Entry(root1,width=20,bd=2)
        entry2.place(x=530,y=30)
        label5 = Label(root1, text='Date Format =dd/mm/yy',font=("times new roman",15,"bold"),bg="light blue",relief=GROOVE)
        label5.place(x=700,y=30)
        #back
        Button3=Button(root1,text="Back", command=root1.destroy,font=("times new roman",18,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button3.place(x=1800,y=30)
        
        #submit
        Button10=Button(root1, text="Submit",command=submit1,font=("times new roman",18,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button10.place(x=1790,y=100)
        #######reset###########
        def reset_values():
            entry2.delete(0, 'end')    
            
        Button4=Button(root1,text="Reset", command=reset_values,font=("times new roman",18,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
        Button4.place(x=1800,y=170) 

        root1.mainloop()

    Button4=Button(root,text="Teacher report",command=teacher_report,font=("times new roman",18,"bold"),bg="light blue",bd=3,relief=GROOVE,activebackground="grey",activeforeground="light blue")
    Button4.place(x=1700,y=90)


    root.mainloop()

def login():
    #login
    conn = psycopg2.connect(database="school", user="harshit", password="1996", host="127.0.0.1", port="5432")
    cursor=conn.cursor()
    temp=0
    user="SELECT * FROM public.user WHERE u_name='"+win.uname.get()+"' and u_pass='"+win.pass_.get()+"'"
    cursor.execute(user)
    rows=cursor.fetchone()       
    if(rows!=None):
        temp=1
    else:
        temp=0    
    if win.uname.get()=="" and win.pass_.get()=="":
        messagebox.showerror("Error","All fields are required!!")   
    if temp==1:
        win.uname.set("")
        win.pass_.set("") 
        report_() 
                  
    if temp==0:
        messagebox.showerror("Error","Invalid username or password")      

               
#win.root.overrideredirect(True)
# win.root.wm_attributes('-type', 'splash')
win.bg_icon=ImageTk.PhotoImage(file="images/a2.jpg")
win.user_icon=PhotoImage(file="images/u.png")
win.pass_icon=PhotoImage(file="images/p.png")
win.logo_icon=PhotoImage(file="images/logo.png")
        
win.uname=StringVar()
win.pass_=StringVar()
        
bg_lbl=Label(win,image=win.bg_icon)
bg_lbl.pack()
               
title=Label(win,text="School A.I ADMIN LOGIN",font=("times new roman",40,"bold"),bg="light blue",fg="grey",bd=10,relief=GROOVE)
title.place(x=620,y=250)

Login_Frame=Frame(win,bg="light blue")
Login_Frame.place(x=700,y=350)
logolbl=Label(Login_Frame,image=win.logo_icon,bd=0)
logolbl.grid(row=0,columnspan=2 ,pady=20)
    
lbluser=Label(Login_Frame,text="Username",image=win.user_icon,compound=LEFT,font=("times new roman",20,"bold"),bg="light blue")
lbluser.grid(row=1,column=0,padx=20,pady=10)
txtuser=Entry(Login_Frame,bd=5,textvariable=win.uname,relief=GROOVE,font=("",15))
txtuser.grid(row=1,column=1,padx=20)
        
lblpass=Label(Login_Frame,text="Password",image=win.pass_icon,compound=LEFT,font=("times new roman",20,"bold"),bg="light blue")
lblpass.grid(row=2,column=0,padx=20,pady=10)
txtpass=Entry(Login_Frame,bd=5,show="*",relief=GROOVE,textvariable=win.pass_,font=("",15))
txtpass.grid(row=2,column=1,padx=20)

btn_log=Button(Login_Frame,text="Login",width=15,command=login,font=("times new roman",14,"bold"),bg="light blue",fg="black")
btn_log.grid(row=3,columnspan=2,pady=10)
btn_quit=Button(Login_Frame,text="Quit",width=15,command=win.destroy,font=("times new roman",14,"bold"),bg="light blue",fg="black")
btn_quit.grid(row=4,columnspan=2,pady=10)
       



win.mainloop()           